﻿using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.HomeAssistant;
using LibreHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace IOTLinkAddon.Service.Monitors
{
    class CPUMonitor : BaseMonitor
    {
        private static readonly string CONFIG_KEY = "CPU";

        private static PerformanceCounter _cpuPerformanceCounter;
        private static IHardware _cpuHardwareMonitor;

        public override void Init()
        {
            if (_cpuPerformanceCounter == null)
                _cpuPerformanceCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");

            if (_cpuHardwareMonitor == null)
            {
                try
                {
                    var computer = new Computer { IsCpuEnabled = true };
                    computer.Open();

                    _cpuHardwareMonitor = computer.Hardware.FirstOrDefault(h => h.HardwareType == HardwareType.Cpu);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("Failed to read temperature", ex);
                }
            }

            _cpuPerformanceCounter.NextValue();
        }

        public override string GetConfigKey()
        {
            return CONFIG_KEY;
        }

        public override List<MonitorItem> GetMonitorItems(Configuration config, int interval)
        {
            List<MonitorItem> result = new List<MonitorItem>();

            // CPU Usage
            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_RAW,
                Topic = "Stats/CPU/Usage",
                Value = Math.Round(_cpuPerformanceCounter.NextValue(), 0),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Id = "Usage",
                    Unit = "%",
                    Name = "CPU Usage",
                    Component = HomeAssistantComponent.Sensor,
                    Icon = "mdi:speedometer"
                }
            });

            // CPU Temperature
            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_RAW,
                Topic = "Stats/CPU/Temperature",
                Value = (int)GetTemperature(),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Id = "Temperature",
                    Unit = "°C",
                    Name = "CPU Temperature",
                    Component = HomeAssistantComponent.Sensor,
                    Icon = "mdi:fire"
                }
            });

            return result;
        }

        private float GetTemperature()
        {
            if (_cpuHardwareMonitor == null)
            {
                return -1;
            }

            try
            {
                _cpuHardwareMonitor.Update();
                var temperatureSensors = _cpuHardwareMonitor.Sensors.Where(s => s.SensorType == SensorType.Temperature).ToList();
                var cpuTempSensor = temperatureSensors?.FirstOrDefault(t => t.Name.ToLower() == "core average") ??
                                    temperatureSensors?.First();
                return cpuTempSensor?.Value ?? 0;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Failed to read temperature", e);
                return 0;
            }
        }
    }
}
