﻿using IOTLinkAddon.Common;
using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.HomeAssistant;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace IOTLinkAddon.Service.Monitors
{
    class SystemMonitor : BaseMonitor
    {
        private static readonly string CONFIG_KEY = "SystemInfo";

        private static readonly Dictionary<string, AddonRequestType> AGENT_REQUESTS = new Dictionary<string, AddonRequestType>
        {
            { "IdleTime", AddonRequestType.REQUEST_IDLE_TIME }
        };

        public override void Init()
        {
            PlatformHelper.LastBootUpTime();
        }

        public override string GetConfigKey()
        {
            return CONFIG_KEY;
        }

        public override Dictionary<string, AddonRequestType> GetAgentRequests()
        {
            return AGENT_REQUESTS;
        }

        public override List<MonitorItem> GetMonitorItems(Configuration config, int interval)
        {
            List<MonitorItem> result = new List<MonitorItem>();

            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_RAW,
                Topic = "Stats/System/CurrentUser",
                Value = PlatformHelper.GetCurrentUsername(),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Component = HomeAssistantComponent.Sensor,
                    Id = "CurrentUsername",
                    Name = "Current Username",
                    Icon = "mdi:account"
                }
            });

            // Boot Time
            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_DATETIME,
                Topic = "Stats/System/BootTime",
                Value = PlatformHelper.LastBootUpTime().ToString(config.GetValue("formats:dateTimeFormat", "yyyy'-'MM'-'dd'T'HH':'mm':'ssK"), CultureInfo.InvariantCulture),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Component = HomeAssistantComponent.Sensor,
                    Id = "BootTime",
                    Name = "System Boot Time",
                    Icon = "mdi:timer-outline",
                    DeviceClass = "timestamp"
                }
            });

            return result;
        }

        public override List<MonitorItem> OnAgentResponse(Configuration config, AddonRequestType type, dynamic data, string username)
        {
            switch (type)
            {
                case AddonRequestType.REQUEST_IDLE_TIME:
                    return ParseIdleTimeInformation(config, data, username);

                default: break;
            }

            return null;
        }

        private List<MonitorItem> ParseIdleTimeInformation(Configuration config, dynamic data, string username)
        {
            List<MonitorItem> result = new List<MonitorItem>();

            var idleTime = DateTime.UtcNow - TimeSpan.FromSeconds((int)data.requestData);
            result.Add(new MonitorItem
            {
                ConfigKey = CONFIG_KEY,
                Type = MonitorItemType.TYPE_DATETIME,
                Topic = "Stats/System/IdleTime",
                Value = idleTime.ToString(config.GetValue("formats:dateTimeFormat", "yyyy'-'MM'-'dd'T'HH':'mm':'ssK"), CultureInfo.InvariantCulture),
                DiscoveryOptions = new HassDiscoveryOptions()
                {
                    Component = HomeAssistantComponent.Sensor,
                    Id = "IdleTime",
                    Name = "System Idle Time",
                    Icon = "mdi:timer-outline",
                    DeviceClass = "timestamp"
                }
            });

            return result;
        }
    }
}
