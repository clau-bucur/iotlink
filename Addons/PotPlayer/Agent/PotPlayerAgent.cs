﻿using System;
using System.Drawing;
using System.Dynamic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IOTLinkAddon.Common;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.Events;

namespace IOTLinkAddon.Agent
{
	public class PotPlayerAgent : AgentAddon
	{
		private const int WM_USER = 0x0400;
		private const int WM_POTPLAY = 0x0111;

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SendMessage(int hWnd, int Msg, int wParam, [Out] int lParam);


		[DllImport("user32.dll", SetLastError = true)]
		public static extern int FindWindow(string lpClassName, string lpWindowName);

		public override void Init(IAddonManager addonManager)
		{
			base.Init(addonManager);

			OnConfigReloadHandler += OnConfigReload;
			OnAgentRequestHandler += OnAgentRequest;
		}

		private void OnConfigReload(object sender, ConfigReloadEventArgs e)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnConfigReload");
		}

		private void OnAgentRequest(object sender, AgentAddonRequestEventArgs e)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnAgentRequest");

			AddonRequestType requestType = e.Data.requestType;
			switch (requestType)
			{
				case AddonRequestType.REQUEST_SET_PLAY_STATUS:
					OnSetPlayStatusMessage((int)e.Data.requestData);
					break;

				case AddonRequestType.REQUEST_SET_VOLUME:
					OnSetVolumeMessage((int)e.Data.requestData);
					break;

				case AddonRequestType.REQUEST_SET_CURRENT_TIME:
					OnSetCurrentTimeMessage((int)e.Data.requestData);
					break;

				case AddonRequestType.REQUEST_CMD_RAW:
					OnRawMessage((int)e.Data.requestData);
					break;

				case AddonRequestType.REQUEST_CMD_PLAYBACK_NEXT:
		        case AddonRequestType.REQUEST_CMD_PLAYBACK_PLAY_PAUSE:
				case AddonRequestType.REQUEST_CMD_PLAYBACK_PREVIOUS:
				case AddonRequestType.REQUEST_CMD_PLAYBACK_STOP:
				case AddonRequestType.REQUEST_CMD_UI_FULLSCREEN:
				case AddonRequestType.REQUEST_CMD_VOL_DOWN:
				case AddonRequestType.REQUEST_CMD_VOL_TOGGLE_MUTE:
				case AddonRequestType.REQUEST_CMD_VOL_UP:
					OnRawMessage((int)requestType);
					break;

				default: break;
			}
			SendPotPlayerStatus();
		}

		private void OnRawMessage(int state)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnRawMessage");
			SendMessage(GetPotPlayerHwnd(), WM_POTPLAY, state, 0);
		}

		private void OnSetPlayStatusMessage(int state)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnSetPlayStatusMessage");
			var currentState = GetPlayStatus();
			if (currentState == 0)
			{
				return;
			}

			switch (state)
			{
				case 0: //toggle
					if (currentState != -1)
					{
						OnRawMessage((int)AddonRequestType.REQUEST_CMD_PLAYBACK_PLAY_PAUSE);
					}
					break;
				
				case 1: //pause
					SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_SET_PLAY_STATUS, 1);
					break;
				
				case 2: //play
					if (currentState == -1) //if stopped, issuing POT_SET_PLAY_STATUS with value 2 won't start playback
					{
						OnRawMessage((int)AddonRequestType.REQUEST_CMD_PLAYBACK_PLAY_PAUSE);
					}
					else
					{
						SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_SET_PLAY_STATUS, 2);
					}
					break;
			}
		}

		private void OnSetVolumeMessage(int volume)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnSetVolumeMessage");
			if (volume >= 0 && volume <= 100)
			{
				SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_SET_VOLUME, volume);
			}
		}

		private void OnSetCurrentTimeMessage(int position)
		{
			LoggerHelper.Verbose("PotPlayerAgent::OnSetCurrentTimeMessage");
			if (position >= 0)
			{
				SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_SET_CURRENT_TIME, position);
			}
		}

		private void SendPotPlayerStatus()
		{
			dynamic addonData = new ExpandoObject();
			addonData.requestType = AddonRequestType.REQUEST_POT_PLAYER_STATUS;
			switch (GetPlayStatus())
			{
				case -1:
					addonData.requestStatus = "on";
					break;

				case 1:
					addonData.requestStatus = "paused";
					break;

				case 2:
					addonData.requestStatus = "playing";
					break;

				default:
					addonData.requestStatus = "off";
					break;
			}
			addonData.requestVolume = GetVolume();
			addonData.requestTotalTime = GetTotalTime();
			addonData.requestCurrentTime = GetCurrentTime();
			addonData.requestFullScreen = GetFullScreen();

			GetManager().SendAgentResponse(this, addonData);
		}

		private int GetPotPlayerHwnd()
		{
			int hwnd = FindWindow("PotPlayer", null);
			if (hwnd == 0)
			{
				hwnd = FindWindow("PotPlayer64", null);
			}

			return hwnd;
		}

		private int GetPlayStatus()
		{
			var status = SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_GET_PLAY_STATUS, 0);
			return status == -1 || status == 1 || status == 2 ? status : 0;
		}

		private int GetVolume()
		{
			return SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_GET_VOLUME, 0);
		}

		private int GetTotalTime()
		{
			return SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_GET_TOTAL_TIME, 0) / 1000;
		}

		private int GetCurrentTime()
		{
			return SendMessage(GetPotPlayerHwnd(), WM_USER, (int)WmUserCommands.POT_GET_CURRENT_TIME, 0) / 1000;
		}

		private bool GetFullScreen()
		{
			int hWnd = GetPotPlayerHwnd();
			if (hWnd > 0)
			{
				GetWindowRect(hWnd, out RECT appBounds);
				var screenBounds = Screen.FromHandle(new IntPtr(hWnd)).Bounds;
				if ((appBounds.Bottom - appBounds.Top) == screenBounds.Height && (appBounds.Right - appBounds.Left) == screenBounds.Width)
				{
					return true;
				}
			}

			return false;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RECT
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;
		}

		[DllImport("user32.dll", SetLastError = true)]
		private static extern int GetWindowRect(int hwnd, out RECT rc);
	}
}
