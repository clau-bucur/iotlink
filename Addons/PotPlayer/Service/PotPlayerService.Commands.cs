﻿using System.Dynamic;
using System.IO;
using IOTLinkAddon.Common;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform;
using IOTLinkAPI.Platform.Events.MQTT;

namespace IOTLinkAddon.Service
{
	public partial class PotPlayerService
	{
        private static readonly string[] PotPlayerDefaultLocations =
        {
            @"C:\Program Files\Daum\PotPlayer\PotPlayerMini.exe",
            @"C:\Program Files (x86)\Daum\PotPlayer\PotPlayerMini.exe",
            @"C:\Program Files\Daum\PotPlayer\PotPlayerMini64.exe",
        };

        private void OnLaunchMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnLaunchMessage: Message received");

            string potPlayerLocation = null;
            var payload = e.Message.GetPayload();
            if (!string.IsNullOrWhiteSpace(payload))
            {
                potPlayerLocation = Path.Combine(payload, "PotPlayerMini.exe");
                if (!File.Exists(potPlayerLocation))
                {
                    potPlayerLocation = Path.Combine(payload, "PotPlayerMini64.exe");
                }
            }
            else
            {
                foreach (var location in PotPlayerDefaultLocations)
                {
                    if (File.Exists(location))
                    {
                        potPlayerLocation = location;
                        break;
                    }
                }
            }
            if (potPlayerLocation == null)
            {
                return;
            }

            PlatformHelper.Run(new RunInfo
            {
                Application = potPlayerLocation,
                WorkingDir = Path.GetDirectoryName(potPlayerLocation)
            });
        }

        private void OnRawMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnRawMessage: Message received");
            if (int.TryParse(e.Message.GetPayload(), out int raw))
            {
                SendRaw(raw);
            }
        }

        private void OnPlayMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnPlayMessage: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_SET_PLAY_STATUS;
            addonData.requestData = 2;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnPauseMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnPauseMessage: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_SET_PLAY_STATUS;
            addonData.requestData = 1;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnPlayPauseMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnPauseMessage: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_CMD_PLAYBACK_PLAY_PAUSE;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnNextMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnNextMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_PLAYBACK_NEXT);
        }

        private void OnPrevMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnPrevMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_PLAYBACK_PREVIOUS);
        }

        private void OnStopMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnStopMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_PLAYBACK_STOP);
        }

        private void OnVolUpMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnVolUpMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_VOL_UP);
        }

        private void OnVolDownMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnVolDownMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_VOL_DOWN);
        }

        private void OnVolMuteMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnVolMuteMessage: Message received");
            SendRaw(AddonRequestType.REQUEST_CMD_VOL_TOGGLE_MUTE);
        }

        private void OnSetCurrentTime(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnSetCurrentTime: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_SET_CURRENT_TIME;
            addonData.requestData = e.Message.GetPayload();
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnSetVolume(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnSetVolume: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_SET_VOLUME;
            addonData.requestData = e.Message.GetPayload();
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnFullscreenMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Verbose("OnFullscreenMessage: Message received");
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_CMD_UI_FULLSCREEN;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void SendRaw(AddonRequestType rawMessage)
        {
            SendRaw((int)rawMessage);
        }
        private void SendRaw(int rawMessage)
        {
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_CMD_RAW;
            addonData.requestData = rawMessage;
            GetManager().SendAgentRequest(this, addonData);
        }
    }
}
