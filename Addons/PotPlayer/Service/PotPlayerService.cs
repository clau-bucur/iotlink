﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Timers;
using IOTLinkAddon.Common;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform;
using IOTLinkAPI.Platform.Events;

namespace IOTLinkAddon.Service
{
    public partial class PotPlayerService : ServiceAddon
	{
        private static readonly int DEFAULT_INTERVAL = 5;

        private Timer _monitorTimer;

        private string _configPath;
        private Configuration _config;

        private readonly Dictionary<string, string> _cache = new Dictionary<string, string>();

        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);

            GetManager().SubscribeTopic(this, "cmd/launch", OnLaunchMessage);
            GetManager().SubscribeTopic(this, "cmd/raw", OnRawMessage);
            GetManager().SubscribeTopic(this, "cmd/play", OnPlayMessage);
            GetManager().SubscribeTopic(this, "cmd/pause", OnPauseMessage);
            GetManager().SubscribeTopic(this, "cmd/playpause", OnPlayPauseMessage);
            GetManager().SubscribeTopic(this, "cmd/next", OnNextMessage);
            GetManager().SubscribeTopic(this, "cmd/prev", OnPrevMessage);
            GetManager().SubscribeTopic(this, "cmd/stop", OnStopMessage);
            GetManager().SubscribeTopic(this, "cmd/vol-up", OnVolUpMessage);
            GetManager().SubscribeTopic(this, "cmd/vol-down", OnVolDownMessage);
            GetManager().SubscribeTopic(this, "cmd/vol-mute", OnVolMuteMessage);
            GetManager().SubscribeTopic(this, "cmd/set-volume", OnSetVolume);
            GetManager().SubscribeTopic(this, "cmd/set-current-time", OnSetCurrentTime);
            GetManager().SubscribeTopic(this, "cmd/fullscreen", OnFullscreenMessage);

            var cfgManager = ConfigurationManager.GetInstance();
            _configPath = Path.Combine(base._currentPath, "config.yaml");
            _config = cfgManager.GetConfiguration(_configPath);
			cfgManager.SetReloadHandler(_configPath, OnConfigReload);

			OnConfigReloadHandler += OnConfigReload;
			OnAgentResponseHandler += OnAgentResponse;
			OnMQTTConnectedHandler += OnClearEvent;
			OnRefreshRequestedHandler += OnClearEvent;

			SetupTimer();
        }

        private void OnClearEvent(object sender, EventArgs e)
        {
            LoggerHelper.Verbose("Event {0} Received. Clearing cache and resending information.", e.GetType().ToString());

            _cache.Clear();
            RequestStatus();
        }

        private void OnConfigReload(object sender, ConfigReloadEventArgs e)
        {
            if (e.ConfigType != ConfigType.CONFIGURATION_ADDON)
            {
                return;
            }

            LoggerHelper.Verbose("Reloading configuration");
            _config = ConfigurationManager.GetInstance().GetConfiguration(_configPath);
            SetupTimer();
        }

        private void SetupTimer()
        {
            if (_config == null || !_config.GetValue("enabled", false))
            {
                LoggerHelper.Info("PotPlayer addon is disabled.");
                if (_monitorTimer != null)
                {
                    _monitorTimer.Stop();
                    _monitorTimer = null;
                }

                return;
            }

            if (_monitorTimer == null)
            {
                _monitorTimer = new Timer();
                _monitorTimer.Elapsed += (object source, ElapsedEventArgs e) => { RequestStatus(); };
            }

            _monitorTimer.Stop();
            _monitorTimer.Interval = GetInterval() * 1000;
            _monitorTimer.Start();

            LoggerHelper.Info("PotPlayer addon is activated.");
        }

        private void RequestStatus()
        {
            if (_monitorTimer == null)
            {
                return;
            }

            try
            {
                _monitorTimer.Stop(); // Stop the timer in order to prevent overlapping

                LoggerHelper.Debug("Requesting Agent information");

                dynamic addonData = new ExpandoObject();
                addonData.requestType = AddonRequestType.REQUEST_POT_PLAYER_STATUS;

				GetManager().SendAgentRequest(this, addonData);
			}
            catch (Exception ex)
            {
                LoggerHelper.Error("RequestStatus - Error: {0}", ex);
            }
            finally
            {
                _monitorTimer.Start(); // After everything, start the timer again.
            }
        }

        private void OnAgentResponse(object sender, AgentAddonResponseEventArgs e)
        {
			switch ((AddonRequestType)e.Data.requestType)
			{
				case AddonRequestType.REQUEST_POT_PLAYER_STATUS:
                    int
                        currentTime = (int)e.Data.requestCurrentTime,
                        totalTime = (int)e.Data.requestTotalTime;

                    PublishValue("Status", (string)e.Data.requestStatus);
                    PublishValue("Volume", (string)e.Data.requestVolume);
                    PublishValue("TotalTime", TransformTime(totalTime));
                    PublishValue("CurrentTime", TransformTime(currentTime));
                    PublishValue("FullScreen", ((bool)e.Data.requestFullScreen).ToString());
                    PublishValue("TotalTimeSec", totalTime.ToString());
                    PublishValue("CurrentTimeSec", currentTime.ToString());
                    break;
            }
        }

        private string TransformTime(int valueInS)
		{
            var timeFormat = GetTimeFormat();
            if (!string.IsNullOrWhiteSpace(timeFormat))
            {
                return TimeSpan.FromSeconds(valueInS).ToString(timeFormat);
            }

            return valueInS.ToString();
        }

        private void PublishValue(string topic, string value)
        {
            if (string.IsNullOrWhiteSpace(topic))
            {
                return;
            }

            if (!string.IsNullOrEmpty(value) && GetCacheable())
            {
                if (_cache.ContainsKey(topic) && _cache[topic].Equals(value))
                {
                    return;
                }

                _cache[topic] = value;
            }

            GetManager().PublishMessage(this, topic, value);
        }
    }
}
