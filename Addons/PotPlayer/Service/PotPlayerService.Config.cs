﻿namespace IOTLinkAddon.Service
{
	public partial class PotPlayerService
	{
        private bool GetCacheable()
        {
            return _config.GetValue("cacheable", false);
        }

        private int GetInterval()
        {
            return _config.GetValue("interval", DEFAULT_INTERVAL);
        }

        private string GetTimeFormat()
        {
            return _config.GetValue("timeFormat", null);
        }

    }
}
