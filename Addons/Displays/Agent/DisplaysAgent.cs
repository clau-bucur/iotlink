﻿using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using IOTLinkAddon.Common;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.Events;
using WindowsDisplayAPI;
using WindowsDisplayAPI.DisplayConfig;
using WindowsDisplayAPI.Exceptions;
using WindowsDisplayAPI.Native.DisplayConfig;

namespace IOTLinkAddon.Agent
{
	public class DisplaysAgent : AgentAddon
	{

		public override void Init(IAddonManager addonManager)
		{
			base.Init(addonManager);

			OnConfigReloadHandler += OnConfigReload;
			OnAgentRequestHandler += OnAgentRequest;
		}

		private void OnConfigReload(object sender, ConfigReloadEventArgs e)
		{
			LoggerHelper.Verbose("DisplaysAgent::OnConfigReload");
		}

		private void OnAgentRequest(object sender, AgentAddonRequestEventArgs e)
		{
			LoggerHelper.Verbose("DisplaysAgent::OnAgentRequest");

			AddonRequestType requestType = e.Data.requestType;
			switch (requestType)
			{
				case AddonRequestType.REQUEST_DISPLAY_INFORMATION:
					SendDisplayInfo();
					break;

				case AddonRequestType.REQUEST_DISPLAY_INTERNAL:
					DisplayInternal();
					break;

				case AddonRequestType.REQUEST_DISPLAY_CLONE:
					DisplayClone();
					break;

				case AddonRequestType.REQUEST_DISPLAY_EXTENDED:
					DisplayExtended();
					break;

				case AddonRequestType.REQUEST_DISPLAY_EXTERNAL:
					DisplayExternal();
					break;

				case AddonRequestType.REQUEST_DISPLAYS_FORCE_FULLHD60HZ:
					DisplaysForceFullHd60Hz();
					break;
			}
		}

		private void DisplayInternal()
		{
			LoggerHelper.Verbose("DisplaysAgent::DisplayInternal");
			try
			{
				PathInfo.ApplyTopology(DisplayConfigTopologyId.Internal, true);
			}
			catch (PathChangeException ex)
			{
				LoggerHelper.Error(ex.Message);
			}
		}

		private void DisplayClone()
		{
			LoggerHelper.Verbose("DisplaysAgent::DisplayClone");
			try
			{
				PathInfo.ApplyTopology(DisplayConfigTopologyId.Clone, true);
			}
			catch (PathChangeException ex)
			{
				LoggerHelper.Error(ex.Message);
			}
		}

		private void DisplayExtended()
		{
			LoggerHelper.Verbose("DisplaysAgent::DisplayExtended");
			try
			{
				PathInfo.ApplyTopology(DisplayConfigTopologyId.Extend, true);
			}
			catch (PathChangeException ex)
			{
				LoggerHelper.Error(ex.Message);
			}
		}

		private void DisplayExternal()
		{
			LoggerHelper.Verbose("DisplaysAgent::DisplayExternal");
			try
			{
				PathInfo.ApplyTopology(DisplayConfigTopologyId.External, true);
			}
			catch (PathChangeException ex)
			{
				LoggerHelper.Error(ex.Message);
			}
		}

		private void DisplaysForceFullHd60Hz()
		{
			LoggerHelper.Verbose("DisplaysAgent::DisplaysForceFullHd60Hz");
			ForceFullHd60Hz();
		}

		private void ForceFullHd60Hz()
		{
			try
			{
				Display.GetDisplays().ToList().ForEach(d => d.SetSettings(new DisplaySetting(new Size(1920, 1080), 60)));
				DisplaySetting.ApplySavedSettings();
			}
			catch (ModeChangeException ex)
			{
				LoggerHelper.Error(ex.Message);
			}
		}

		private void SendDisplayInfo()
		{
			dynamic addonData = new ExpandoObject();
			addonData.requestType = AddonRequestType.REQUEST_DISPLAY_INFORMATION;
			addonData.requestData = GetDisplays();
			GetManager().SendAgentResponse(this, addonData);
		}

		public static List<DisplayData> GetDisplays()
		{
			var displays = Display.GetDisplays();
			var result = new List<DisplayData>();
			foreach (var display in displays)
			{
				var target = display.ToPathDisplayTarget();
				result.Add(new DisplayData
				{
					BitsPerPixel = (int)display.CurrentSetting.ColorDepth,
					DeviceName = display.DeviceName,
					DisplayFriendlyName = string.IsNullOrWhiteSpace(target.FriendlyName) ? "Generic" : target.FriendlyName,
					Frequency = display.CurrentSetting.Frequency,
					Resolution = display.CurrentSetting.Resolution,
					DpiScale = (int)display.ToPathDisplaySource().CurrentDPIScale,
					IsPrimaryDisplay = display.IsGDIPrimary
				});
			}

			return result;
		}

	}
}
