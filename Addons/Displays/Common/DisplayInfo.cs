﻿using System.Drawing;

namespace IOTLinkAddon.Common
{
    public class DisplayData
    {
	    public string DeviceName { get; set; }
        public string DisplayFriendlyName { get; set; }
        public bool IsPrimaryDisplay { get; set; }
        public Size Resolution { get; set; }
        public Size ActiveResolution { get; set; }
        public int DpiScale { get; set; }

        public int Frequency { get; set; }
        public int BitsPerPixel { get; set; }

        public int ScreenHeight => Resolution.Height;
        public int ScreenWidth => Resolution.Width;

        public override string ToString()
        {
            return $"{DisplayFriendlyName}{(IsPrimaryDisplay ? "*" : string.Empty)} [{Resolution.Width}x{Resolution.Height}@{Frequency}Hz]";
        }
    }
}
