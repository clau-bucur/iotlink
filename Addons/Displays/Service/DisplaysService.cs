﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Timers;
using IOTLinkAddon.Common;
using IOTLinkAPI.Addons;
using IOTLinkAPI.Configs;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform;
using IOTLinkAPI.Platform.Events;

namespace IOTLinkAddon.Service
{
	public partial class DisplaysService : ServiceAddon
	{
        private static readonly int DEFAULT_INTERVAL = 5;

        private Timer _monitorTimer;

        private string _configPath;
        private Configuration _config;

        private readonly Dictionary<string, string> _cache = new Dictionary<string, string>();

        /// <summary>
        /// Store the number of displays that were detected on last run.
        /// If in the meantime some displays get disconnected, using this will help to clear out the MQTT topics.
        /// </summary>
        private int _lastDisplaysCount;

        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);

            GetManager().SubscribeTopic(this, "cmd/internal", OnDisplaysInternalMessage);
            GetManager().SubscribeTopic(this, "cmd/clone", OnDisplaysCloneMessage);
            GetManager().SubscribeTopic(this, "cmd/extended", OnDisplaysExtendedMessage);
            GetManager().SubscribeTopic(this, "cmd/external", OnDisplayExternalMessage);
            GetManager().SubscribeTopic(this, "cmd/force-fullhd-60hz", OnDisplaysForceFullHd60HzMessage);

            var cfgManager = ConfigurationManager.GetInstance();
            _configPath = Path.Combine(base._currentPath, "config.yaml");
            _config = cfgManager.GetConfiguration(_configPath);
			cfgManager.SetReloadHandler(_configPath, OnConfigReload);

			OnConfigReloadHandler += OnConfigReload;
			OnAgentResponseHandler += OnAgentResponse;
			OnMQTTConnectedHandler += OnClearEvent;
			OnRefreshRequestedHandler += OnClearEvent;

			SetupTimer();
        }

        private void OnClearEvent(object sender, EventArgs e)
        {
            LoggerHelper.Verbose("Event {0} Received. Clearing cache and resending information.", e.GetType().ToString());

            _cache.Clear();
            RequestStatus();
        }

        private void OnConfigReload(object sender, ConfigReloadEventArgs e)
        {
            if (e.ConfigType != ConfigType.CONFIGURATION_ADDON)
            {
                return;
            }

            LoggerHelper.Verbose("Reloading configuration");
            _config = ConfigurationManager.GetInstance().GetConfiguration(_configPath);
            SetupTimer();
        }

        private void SetupTimer()
        {
            if (_config == null || !_config.GetValue("enabled", false))
            {
                LoggerHelper.Info("Displays addon is disabled.");
                if (_monitorTimer != null)
                {
                    _monitorTimer.Stop();
                    _monitorTimer = null;
                }

                return;
            }

            if (_monitorTimer == null)
            {
                _monitorTimer = new Timer();
                _monitorTimer.Elapsed += (object source, ElapsedEventArgs e) => { RequestStatus(); };
            }

            _monitorTimer.Stop();
            _monitorTimer.Interval = GetInterval() * 1000;
            _monitorTimer.Start();

            LoggerHelper.Info("Displays addon is activated.");
        }

        private void RequestStatus()
        {
			if (_monitorTimer == null)
			{
				return;
			}

			try
			{
				_monitorTimer.Stop(); // Stop the timer in order to prevent overlapping

				LoggerHelper.Debug("Requesting Agent information");

				dynamic addonData = new ExpandoObject();
				addonData.requestType = AddonRequestType.REQUEST_DISPLAY_INFORMATION;

				GetManager().SendAgentRequest(this, addonData);
			}
			catch (Exception ex)
			{
				LoggerHelper.Error("RequestStatus - Error: {0}", ex);
			}
			finally
			{
				_monitorTimer.Start(); // After everything, start the timer again.
			}
		}

        private void OnAgentResponse(object sender, AgentAddonResponseEventArgs e)
        {
			switch ((AddonRequestType)e.Data.requestType)
			{
				case AddonRequestType.REQUEST_DISPLAY_INFORMATION:
					foreach ((string, string) item in ParseDisplayInformation(e.Data))
					{
                        PublishValue(item.Item1, item.Item2);
					}
					break;
			}
		}

		private List<(string, string)> ParseDisplayInformation(dynamic data)
		{
			LoggerHelper.Verbose("DisplaysService - Received Display Informations");

			var result = new List<(string, string)>();
			var displayInfos = data.requestData.ToObject<List<DisplayData>>();

			for (var i = 0; i < Math.Max(displayInfos.Count, _lastDisplaysCount); i++)
			{
				var displayInfo = i < displayInfos.Count ? displayInfos[i] : null;
				var topic = string.Format("Display{0}", i);

                //add empty message (payload) for disconnected displays -> will clear the topic indicading that the display is no longer available
				result.Add((topic + "/ScreenWidth", displayInfo?.ScreenWidth.ToString() ?? string.Empty));
                result.Add((topic + "/ScreenHeight", displayInfo?.ScreenHeight.ToString() ?? string.Empty));
                result.Add((topic + "/Frequency", displayInfo?.Frequency.ToString() ?? string.Empty));
                result.Add((topic + "/DPI", displayInfo?.DpiScale.ToString() ?? string.Empty));
                result.Add((topic + "/Stat", displayInfo?.ToString() ?? string.Empty));
			}
            _lastDisplaysCount = displayInfos.Count;

            return result;
		}


		private void PublishValue(string topic, string value)
        {
            if (string.IsNullOrWhiteSpace(topic))
            {
                return;
            }

            if (!string.IsNullOrEmpty(value) && GetCacheable())
            {
                if (_cache.ContainsKey(topic) && _cache[topic].Equals(value))
                {
                    return;
                }

                _cache[topic] = value;
            }

            //PublishMessage needs to allow sending empty values
            GetManager().PublishMessage(this, topic, value);
        }
    }
}
