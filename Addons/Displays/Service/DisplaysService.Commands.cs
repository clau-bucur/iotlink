﻿using System.Dynamic;
using IOTLinkAddon.Common;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform.Events.MQTT;

namespace IOTLinkAddon.Service
{
	public partial class DisplaysService
    {
        private void OnDisplaysInternalMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Debug("OnDisplaysInternalMessage: Message received");

            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_DISPLAY_INTERNAL;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnDisplaysCloneMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Debug("OnDisplaysCloneMessage: Message received");

            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_DISPLAY_CLONE;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnDisplaysExtendedMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Debug("OnDisplaysExtendedMessage: Message received");

            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_DISPLAY_EXTENDED;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnDisplayExternalMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Debug("OnDisplayExternalMessage: Message received");

            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_DISPLAY_EXTERNAL;
            GetManager().SendAgentRequest(this, addonData);
        }

        private void OnDisplaysForceFullHd60HzMessage(object sender, MQTTMessageEventEventArgs e)
        {
            LoggerHelper.Debug("OnDisplaysForceFullHd60HzMessage: Message received");

            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_DISPLAYS_FORCE_FULLHD60HZ;
            GetManager().SendAgentRequest(this, addonData);
        }
    }
}
