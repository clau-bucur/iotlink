﻿namespace IOTLinkAddon.Service
{
	public partial class DisplaysService
    {
        private bool GetCacheable()
        {
            return _config.GetValue("cacheable", false);
        }

        private int GetInterval()
        {
            return _config.GetValue("interval", DEFAULT_INTERVAL);
        }
    }
}
