﻿using System;

namespace IOTLinkAPI.Platform.Events
{
    public class InterAddonMessageEventArgs : EventArgs
    {
        public string AddonSource { get; set; }
        public string AddonDestination { get; set; }
        public MessageType Message { get; set; }
    }

    public enum MessageType
    {
        PowerPlanChanged,
    }
}
