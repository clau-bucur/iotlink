﻿using System;
using System.Runtime.InteropServices;
using System.Linq;

namespace IOTLinkAPI.Helpers
{

    public class PowerPlan
    {
        public readonly string Name;
        public Guid Guid;

        public PowerPlan(Guid guid)
        {
            Name = GetPowerPlanName(guid);
            Guid = guid;
        }

        private static string GetPowerPlanName(Guid guid)
        {
            string name = string.Empty;
            IntPtr lpszName = (IntPtr)null;
            uint dwSize = 0;

            PowerReadFriendlyName((IntPtr)null, ref guid, (IntPtr)null, (IntPtr)null, lpszName, ref dwSize);
            if (dwSize > 0)
            {
                lpszName = Marshal.AllocHGlobal((int)dwSize);
                if (0 == PowerReadFriendlyName((IntPtr)null, ref guid, (IntPtr)null, (IntPtr)null, lpszName, ref dwSize))
                {
                    name = Marshal.PtrToStringUni(lpszName);
                }
                if (lpszName != IntPtr.Zero)
                    Marshal.FreeHGlobal(lpszName);
            }

            return name;
        }

        [DllImport("powrprof.dll", EntryPoint = "PowerReadFriendlyName")]
        private static extern uint PowerReadFriendlyName(IntPtr RootPowerKey, ref Guid SchemeGuid, IntPtr SubGroupOfPowerSettingsGuid, IntPtr PowerSettingGuid, IntPtr Buffer, ref uint BufferSize);
    }

    public class PowerManager
    {
        /// <summary>
        /// Indicates that almost no power savings measures will be used.
        /// </summary>
        private static readonly PowerPlan MaximumPerformance;

        /// <summary>
        /// Indicates that fairly aggressive power savings measures will be used.
        /// </summary>
        private static readonly PowerPlan Balanced;

        /// <summary>
        /// Indicates that very aggressive power savings measures will be used to help
        /// stretch battery life.
        /// </summary>
        private static readonly PowerPlan PowerSaver;

        private static PowerPlan[] PowerPlans;

        static PowerManager()
        {
            // See GUID values in WinNT.h.
            MaximumPerformance = new PowerPlan(new Guid("8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c"));
            Balanced = new PowerPlan(new Guid("381b4222-f694-41f0-9685-ff5bb260df2e"));
            PowerSaver = new PowerPlan(new Guid("a1841308-3541-4fab-bc81-f71556f20b4a"));

            PowerPlans = new PowerPlan[]
            {
                MaximumPerformance,
                Balanced,
                PowerSaver
            };
        }

        public static void SetActive(PowerPlan plan)
        {
            PowerSetActiveScheme(IntPtr.Zero, ref plan.Guid);
        }

        public static void SetActive(string powerPlanName)
        {
            if (string.IsNullOrWhiteSpace(powerPlanName))
            {
                return;
            }

            powerPlanName = powerPlanName.Trim().Replace(" ", "");
            var plan = PowerPlans.FirstOrDefault(p => powerPlanName.Equals(p.Name.Trim().Replace(" ", ""), StringComparison.OrdinalIgnoreCase));
            if (plan != null)
            {
                PowerSetActiveScheme(IntPtr.Zero, ref plan.Guid);
            }
        }

        private static Guid GetActiveGuid()
        {
            Guid ActiveScheme = Guid.Empty;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)));
            if (PowerGetActiveScheme((IntPtr) null, out ptr) == 0)
            {
                ActiveScheme = (Guid) Marshal.PtrToStructure(ptr, typeof(Guid));
                if (ptr != null)
                {
                    Marshal.FreeHGlobal(ptr);
                }
            }
            return ActiveScheme;
        }

        public static PowerPlan CurrentPlan
        {
            get
            {
                return PowerPlans.FirstOrDefault(p => p.Guid == GetActiveGuid());
            }
        }

        [DllImport("powrprof.dll", EntryPoint = "PowerSetActiveScheme")]
        public static extern uint PowerSetActiveScheme(IntPtr UserPowerKey, ref Guid ActivePolicyGuid);

        [DllImport("powrprof.dll", EntryPoint = "PowerGetActiveScheme")]
        public static extern uint PowerGetActiveScheme(IntPtr UserPowerKey, out IntPtr ActivePolicyGuid);
    }
}
