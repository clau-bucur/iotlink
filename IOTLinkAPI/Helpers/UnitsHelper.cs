﻿using System;

namespace IOTLinkAPI.Helpers
{
    public abstract class UnitsHelper
    {
        public static double ConvertSize(long sizeInBytes, ref string format)
        {
            switch (format.ToUpper())
            {
                default:
                case "AUTO":
                    return Auto(sizeInBytes, ref format);
                case "B":
                    return sizeInBytes;
                case "KB":
                    return BytesToKilobytes(sizeInBytes);
                case "MB":
                    return BytesToMegabytes(sizeInBytes);
                case "GB":
                    return BytesToGigabytes(sizeInBytes);
                case "TB":
                    return BytesToTerabytes(sizeInBytes);
            }
        }

        public static double Auto(long bytes, ref string format)
        {
            if (bytes >= 0x1000000000000000) // Exabyte
            {
                format = "EB";
                bytes >>= 50;
            }
            else if (bytes >= 0x4000000000000) // Petabyte
            {
                format = "PB";
                bytes >>= 40;
            }
            else if (bytes >= 0x10000000000) // Terabyte
            {
                format = "TB";
                bytes >>= 30;
            }
            else if (bytes >= 0x40000000) // Gigabyte
            {
                format = "GB";
                bytes >>= 20;
            }
            else if (bytes >= 0x100000) // Megabyte
            {
                format = "MB";
                bytes >>= 10;
            }
            else if (bytes >= 0x400) // Kilobyte
            {
                format = "KB";
            }
            else
            {
                format = "B";
                return bytes;
            }

            return bytes / 1024.0;
        }

        public static double BytesToKilobytes(long bytes)
        {
            return Math.Round(bytes / 1024f, 2);
        }

        public static double BytesToMegabytes(long bytes)
        {
            return Math.Round(bytes / (1024f * 1024f), 2);
        }

        public static double BytesToGigabytes(long bytes)
        {
            return Math.Round(bytes / (1024f * 1024f * 1024f), 2);
        }

        public static double BytesToTerabytes(long bytes)
        {
            return Math.Round(bytes / (1024f * 1024f * 1024f * 1024f), 2);
        }

        public static double TerabytesToBytes(long terabytes)
        {
            return Math.Round(terabytes * (1024f * 1024f * 1024f * 1024f), 2);
        }

        public static double GigabytesToBytes(long gigabytes)
        {
            return Math.Round(gigabytes * (1024f * 1024f * 1024f), 2);
        }

        public static double MegabytesToBytes(long megabytes)
        {
            return Math.Round(megabytes * (1024f * 1024f), 2);
        }

        public static double KilobytesToBytes(long kilobytes)
        {
            return Math.Round(kilobytes * 1024f, 2);
        }
    }
}
