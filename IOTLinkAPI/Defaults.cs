﻿using System;

namespace IOTLinkAPI.Helpers
{
    public static class Defaults
    {
        public const int DEFAULT_INTERVAL = 60;

        public const string DEFAULT_FORMAT_DISK_SIZE = "GB";
        public const string DEFAULT_FORMAT_MEMORY_SIZE = "MB";

        public const string DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
        public const string DEFAULT_FORMAT_TIME = "HH:mm:ss";
        public const string DEFAULT_FORMAT_DATETIME = "yyyy'-'MM'-'dd'T'HH':'mm':'ssK";
        public const string DEFAULT_FORMAT_UPTIME = @"dd\d\ hh\:mm\:ss";

        public const string PAYLOAD_ON = "ON";
        public const string PAYLOAD_OFF = "OFF";
    }
}
